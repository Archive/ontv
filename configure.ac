AC_INIT([OnTV],[3.2.0],[ontv])
AC_CONFIG_HEADERS(config.h)
AC_CONFIG_MACRO_DIR([m4])

AM_INIT_AUTOMAKE([foreign])
m4_ifdef([AM_SILENT_RULES], [AM_SILENT_RULES([yes])])
AM_MAINTAINER_MODE
AM_PROG_LIBTOOL
AC_SUBST(ACLOCAL_AMFLAGS, "$ACLOCAL_FLAGS -I m4")
AC_PROG_CC

dnl ==========
dnl pkg-config
dnl ==========

AC_CHECK_PROG(HAVE_PKGCONFIG, pkg-config, yes, no)
if test "x$HAVE_PKGCONFIG" = "xno"; then
    AC_MSG_ERROR(You need to have pkgconfig installed!)
fi

dnl ==================
dnl Translation & I18N
dnl ==================

ALL_LINGUAS="`grep -v '^#' "$srcdir/po/LINGUAS"`"
AC_SUBST([CONFIG_STATUS_DEPENDENCIES],['$(top_srcdir)/po/LINGUAS'])
GETTEXT_PACKAGE=ontv
IT_PROG_INTLTOOL([0.33])
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE, "$GETTEXT_PACKAGE", [The gettext package])
AM_GLIB_GNU_GETTEXT

dnl =====
dnl GConf
dnl =====

AC_PATH_PROG(GCONFTOOL, gconftool-2)
AM_GCONF_SOURCE_2

dnl ======
dnl Python
dnl ======

AM_PATH_PYTHON(2.5)

dnl ============
dnl Requirements
dnl ============

AC_ARG_ENABLE([gnome-applet],
  AS_HELP_STRING([--enable-gnome-applet], [Build applet for gnome panel]),
  [gnome_applet_enable="${enableval}"],
  [gnome_applet_enable="no"])

AS_IF([test "x$gnome_applet_enable" = "xyes"], [
PKG_CHECK_MODULES([GNOME_PYTHON], [gnome-python-2.0 >= 2.16.0])
])
AM_CONDITIONAL(GNOME_APPLET, [test "x$gnome_applet_enable" = "xyes"])

AC_MSG_CHECKING([for vte module])
if AC_RUN_LOG([$PYTHON -c '
try:
    import vte
except ImportError, e:
    if str(e).find("vte") >= 0:
        raise
except:
    pass
']); then
    AC_MSG_RESULT([yes])
else
    AC_MSG_RESULT([no])
    AC_MSG_ERROR([vte Python module required to build ontv])
fi


AC_MSG_CHECKING(for pygtk defs)
PYGTK_DEFSDIR=`$PKG_CONFIG --variable=defsdir pygtk-2.0`
AC_SUBST(PYGTK_DEFSDIR)
AC_MSG_RESULT($PYGTK_DEFSDIR)

dnl ==========================
dnl Control-Center Keybindings
dnl ==========================

AC_MSG_CHECKING([whether the GNOME control-center supports custom keybindings])
if $PKG_CONFIG --variable=keysdir gnome-keybindings > /dev/null; then
    AC_MSG_RESULT([yes])
    KEYBINDINGS_DIR="`$PKG_CONFIG --variable=keysdir gnome-keybindings`"
    AC_SUBST(KEYBINDINGS_DIR)
else
    AC_MSG_RESULT([no])
    KEYBINDINGS_DIR=""
fi
AM_CONDITIONAL([CUSTOM_KEYBINDINGS],[test -n "$KEYBINDINGS_DIR"])

AC_OUTPUT([
Makefile
data/Makefile
data/images/Makefile
ontv/__init__.py
ontv/Makefile
bin/Makefile
po/Makefile.in
])
