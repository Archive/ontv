#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2010 Olof Kindgren <olki@src.gnome.org>

# This file is part of OnTV.

# OnTV is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# OnTV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with OnTV; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

import pygtk
pygtk.require('2.0')
import gtk

import sys
import os.path
PYTHON_DIR = "@PYTHONDIR@"
sys.path.insert(0, os.path.abspath(PYTHON_DIR))

from ontv import DATA_DIR, NAME, LOCALE_DIR
from ontv.ontv_core import OnTVCore
import ontv.gui
from optparse import OptionParser

import locale
locale.setlocale(locale.LC_ALL, '')
locale.bindtextdomain(NAME.lower(), LOCALE_DIR)
import gettext
gettext.bindtextdomain(NAME.lower(), LOCALE_DIR)
gettext.textdomain(NAME.lower())
_ = gettext.gettext


class OnTVStatusIcon:

  def __init__(self, configure = False, debug = False):

    self.ontv_core = OnTVCore(configure, debug, self.cb_status)

    self.builder = gtk.Builder()
    self.builder.set_translation_domain(NAME.lower())
    ui_file = os.path.join(DATA_DIR, "status_icon.ui")
    self.builder.add_from_file(ui_file)
    self.builder.connect_signals(self)

    self.statusIcon = self.builder.get_object("status_icon")

    #Why won't Glade do this?
    menu = self.builder.get_object("popup_menu")
    self.statusIcon.connect('popup-menu', self.popup_menu_cb, menu)

  def update_listings(self, widget):
    self.ontv_core.update_listings()

  def show_preferences(self, widget):
    self.ontv_core.show_preferences_dialog()

  def show_search(self, widget):
    self.ontv_core.show_search_dialog()

  def show_about(self,widget):
    self.ontv_core.show_about_dialog()

  def show_program_window(self,widget):
    self.ontv_core.set_program_window_position(self.get_docking_data())
    self.ontv_core.toggle_program_window()
    
  def get_docking_data(self):

      #FIXME: Size not correct on first view
      screen, pos, ori = self.statusIcon.get_geometry()
      w,h =  self.ontv_core.get_program_window_size()
      if pos[0]+w < gtk.gdk.screen_width():
        x = pos[0]
      else:
        x = gtk.gdk.screen_width()-w
      y = pos[1] + pos[3]
      return (x, y, gtk.gdk.GRAVITY_NORTH_EAST)
    
      #FIXME: Move to left/up if menu doesn't fit
    
  def quit_cb(self, widget, data = None):
    gtk.main_quit()

  def cb_status(self, msg):
    self.statusIcon.set_tooltip(msg)

  def run(self):
    gtk.gdk.threads_init()
    self.ontv_core.run()
    gtk.main()

  def popup_menu_cb(self, widget, button, time, data = None):
    if button == 3:
      if data:
        data.show_all()
        data.popup(None, None, gtk.status_icon_position_menu,
                   3, time, self.statusIcon)

if __name__ == "__main__":
  parser = OptionParser()
  parser.add_option("-c", "--configure",
                    action="store_true",
                    help=_("run XMLTV assistant on startup"))
  parser.add_option("-d", "--debug",
                    action="store_true",
                    help=_("enable debug messages"))

  (options, args) = parser.parse_args()
  ontv_status_icon = OnTVStatusIcon(options.configure, options.debug)
  ontv_status_icon.run()
