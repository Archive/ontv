# -*- coding: utf-8 -*-

# Copyright (C) 2010 Olof Kindgren <olki@src.gnome.org>

# This file is part of OnTV.

# OnTV is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# OnTV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with OnTV; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

import os.path
from gettext import gettext as _

import dbus
import dbus.mainloop.glib

import gui
from assistant import XMLTVAssistant
from config import Configuration
from dbus_service import DBusService
from dialogs import AboutDialog, PreferencesDialog, SearchDialog
from reminders import Reminders
from window import ProgramWindow
from xmltv_file import XMLTVFile
from ontv import NAME, VERSION, UI_DIR, LOCALE_DIR

class OnTVCore:
    """Entry point for OnTV backend"""

    def __init__(self, configure, debug, cb_status):

        self.configure = configure
        self.debug = debug
        self.config = Configuration(self.debug)

        self.xmltvfile = XMLTVFile(self.config)
        self.cb_status = cb_status
        self.reminders = Reminders(self.config)
        if os.path.exists(self.reminders.file):
            self.reminders = self.reminders.load()

        self.pw = ProgramWindow(self.xmltvfile)
        self.sd = SearchDialog(self.xmltvfile, self.reminders)
        self.pd = PreferencesDialog(self.config, self.xmltvfile,
                                    self.reminders, self.pw, self.sd)

        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        remote_object = self.__get_running_instance()

        if remote_object:
            print "OnTV is already running"
            exit()
        else:
            DBusService(self)

    def update_listings(self):
        self.xmltvfile.download()

    def show_about_dialog(self):
        AboutDialog()

    def show_preferences_dialog(self):
        self.pd.show()

    def show_search_dialog(self):
        self.sd.show()

    def toggle_program_window(self):
        visible = self.pw.is_visible()
        if visible:
            self.pw.hide_window()
        else:
            self.pw.show_window(self.window_position)
        return visible

    def __get_running_instance(self):
        session_bus = dbus.SessionBus()
        dbus_object = session_bus.get_object('org.freedesktop.DBus',
                                     '/org/freedesktop/DBus')
        dbus_iface = dbus.Interface(dbus_object, 'org.freedesktop.DBus')
        services = dbus_iface.ListNames()
        if "org.gnome.OnTV" in services:
            return session_bus.get_object("org.gnome.OnTV","/DBusService")
        return False

    def run(self):
        if self.configure or self.config.grabber_command == '':
            xmltv_assistant = XMLTVAssistant(self.config, self.xmltvfile)
            xmltv_assistant.show()
        else:
            self.xmltvfile.connect("downloading", self.__xmltvfile_activity,
                                   _("Downloading TV Listings..."))
            self.xmltvfile.connect("downloading-done",
                                   self.__xmltvfile_downloading_done)
            self.xmltvfile.connect("sorting", self.__xmltvfile_activity,
                                   _("Sorting TV Listings..."))
            self.xmltvfile.connect("sorting-done",
                                   self.__xmltvfile_sorting_done)
            self.xmltvfile.connect("loading", self.__xmltvfile_activity,
                                   _("Loading TV Listings..."))
            self.xmltvfile.connect("loading-done",
                                   self.__xmltvfile_loading_done)
            self.xmltvfile.load()

    def __xmltvfile_activity(self, xmltvfile, activity):
        self.cb_status(activity)

    def __xmltvfile_downloading_done(self, xmltvfile, pid, condition):
        self.cb_status("")
        self.xmltvfile.sort()

    def __xmltvfile_sorting_done(self, xmltvfile, pid, condition):
        self.cb_status("")
        self.xmltvfile.load()

    def __xmltvfile_loading_done(self, xmltvfile, listings):
        self.cb_status("")

    def get_program_window_size(self):
        return self.pw.get_window_size()

    def set_program_window_position(self, window_position):
        self.window_position = window_position

# vim: set sw=4 et sts=4 tw=79 fo+=l:
