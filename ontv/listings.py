# -*- coding: utf-8 -*-

# Copyright (C) 2004-2010 Johan Svedberg <johan@svedberg.com>

# This file is part of OnTV.

# OnTV is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# OnTV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with OnTV; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

import cPickle
import os.path
import thread
from gettext import gettext as _

class Listings(object):
    """Singleton representing the listings"""

    instance = None

    def __new__(type, *args):
        if Listings.instance is None:
            Listings.instance = object.__new__(type)
            Listings.instance.__init(*args)
        return Listings.instance

    def __init(self, *args):
        self.config = args[0]
        self.file = os.path.join(self.config.base_dir, "listings.p")
        self._channels = {}
        self.selected_channels = []

        self.mtime = None

    def __getstate__(self):
        dict = self.__dict__.copy()
        del dict["config"]
        return dict

    def __setstate__(self, listings):
        self.__dict__.update(listings)

    def channel_has_custom_logo(self, channel):
        if self._channels.has_key(channel.name):
            return self._channels[channel.name].custom_logo
        return False

    def add_channel(self, channel):
        if self._channels.has_key(channel.name):
            if self._channels[channel.name].logo_file != "":
                channel.logo_file = self._channels[channel.name].logo_file
                channel.set_logo(self._channels[channel.name].logo_file)
            if self._channels[channel.name].custom_logo:
                channel.custom_logo = self._channels[channel.name].custom_logo
            if channel.name in self.selected_channels:
                channel.selected = True
        self._channels[channel.name] = channel

    def add_program(self, program, channel_name):
        if self._channels.has_key(channel_name):
            program.channel = self._channels[channel_name]
            self._channels[channel_name].programs.append(program)

    def get_channel(self, channel_name):
        if channel_name == "All":
            return self._channels
        if self._channels.has_key(channel_name):
            return self._channels[channel_name]
        return None

    def __save_in_thread(self):
        file = open(self.file, 'w')
        cPickle.dump(self, file, cPickle.HIGHEST_PROTOCOL)
        file.close()
        if self.config.debug:
            print("Saved listings file: %s" % self.file)

    def save(self):
        thread.start_new_thread(self.__save_in_thread, ())

    def load(self):
        file = open(self.file)
        self = cPickle.load(file)
        file.close()
        if self.config.debug:
            print("Loaded listings from file: %s" % self.file)
        return self

    def set_channel_selected(self, channel, selected):
        if selected:
            self.selected_channels.append(channel)
        else:
            self.selected_channels.remove(channel)

    def search_program(self, search_terms, channel):
        matches = []
        for term in search_terms:
            if channel == _("All"):
                for channel in self._channels:
                    for program in self._channels[channel].programs:
                        if term in program.title.lower():
                            matches.append(program)
            else:
                for program in self._channels[channel].programs:
                    if term in program.title.lower():
                        matches.append(program)
        return matches


# vim: set sw=4 et sts=4 tw=79 fo+=l:
