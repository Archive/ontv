# -*- coding: utf-8 -*-

# Copyright (C) 2004-2010 Johan Svedberg <johan@svedberg.com>

# This file is part of OnTV.

# OnTV is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# OnTV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with OnTV; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

import datetime
import re

import gobject

class Program:
    def __init__(self, program):
        self.title = self.__get_title(program)
        self.markup_escaped_title = gobject.markup_escape_text(self.title)
        self.description = self.__get_description(program)
        start = self.__get_datetime(program, "start")
        stop = self.__get_datetime(program, "stop")
        self.start_time = start.strftime("%X")[:5]
        self.stop_time = stop.strftime("%X")[:5]
        self.date = start.strftime("%a, %b %d").encode("utf-8")
        self.time_span = TimeSpan(start, stop)

    def __cmp__(self, other):
        return cmp(self.time_span.start, other.time_span.start)

    def __str__(self):
        return self.title

    def __get_title(self, program):
        return unicode(program["title"][0][0]).encode("utf-8")

    def __get_description(self, program):
        if program.has_key("desc"):
            description = unicode(program["desc"][0][0]).encode("utf-8")
            return description
        return ""

    def __get_datetime(self, program, time):
        time_re = re.compile(r"(\d{4})(\d\d)(\d\d)(\d\d)(\d\d)")
        match = time_re.match(program[time])
        ints = [int(x) for x in match.groups()]
        return datetime.datetime(*ints)

    def get_time_until_start(self):
        return self.time_span.time_until_start()

    def get_time_until_stop(self):
        return self.time_span.time_until_stop()

class TimeSpan:
    """A TimeSpan represents a span of time, for example the running time of a
    specific TV program."""

    def __init__(self, start, stop):
        """Create a TimeSpan from a start time and an stop time."""
        self.start = start
        self.stop = stop

    def spans_time(self, time):
        """Check if this TimeSpan includes a specific time."""
        return time > self.start and time < self.stop

    def spans_now(self):
        """Check if this TimeSpan includes the current time."""
        return self.spans_time(datetime.datetime.now())

    def duration(self):
        """Calculate the distance (TimeDelta) from the start of this TimeSpan,
        to the stop of this TimeSpan."""
        return self.stop - self.start

    def time_until_start_from(self, base):
        """Calculate the distance (TimeDelta) from a specific time, to the
        start of this TimeSpan."""
        return self.start - base

    def time_until_start(self):
        """Calculate the distance (TimeDelta) from the current time, to the
        start of this TimeSpan."""
        return self.time_until_start_from(datetime.datetime.now())

    def time_from_start_from(self, base):
        """Calculate the distance (TimeDelta) from the start of this TimeSpan,
        to a specific time."""
        return base - self.start

    def time_from_start(self):
        """Calculate the distance (TimeDelta) from the start of this TimeSpan,
        to the current time."""
        return self.time_from_start_from(datetime.datetime.now())

    def time_until_stop_from(self, base):
        """Calculate the distance (TimeDelta) from a specific time, to the stop
        of this TimeSpan."""
        return self.stop - base

    def time_until_stop(self):
        """Calculate the distance (TimeDelta) from the current time, to the
        stop of this TimeSpan."""
        return self.time_until_stop_from(datetime.datetime.now())

# vim: set sw=4 et sts=4 tw=79 fo+=l:
