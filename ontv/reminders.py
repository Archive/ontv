# -*- coding: utf-8 -*-

# Copyright (C) 2004-2010 Johan Svedberg <johan@svedberg.com>

# This file is part of OnTV.

# OnTV is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# OnTV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with OnTV; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

import cPickle
import os.path
import thread
from gettext import gettext as _

class Reminders(object):
    """Singleton representing the reminders"""

    instance = None

    def __new__(type, *args):
        if Reminders.instance is None:
            Reminders.instance = object.__new__(type)
            Reminders.instance.__init(*args)
        return Reminders.instance

    def __init(self, *args):
        self.config = args[0]
        self.file = os.path.join(self.config.base_dir, "reminders.p")
        self.reminders = []

    def __getstate__(self):
        dict = self.__dict__.copy()
        del dict["config"]
        return dict

    def __setstate__(self, reminders):
        self.__dict__.update(reminders)

    def has_reminder(self, reminder, check_channel=True):
        program = reminder.program.strip().lower()
        channel = reminder.channel.strip().lower()
        for r in self.reminders:
            if program.find(r.program.strip().lower()) != -1 and \
               reminder.notify_time <= r.notify_time \
               and reminder.notify_time > 0:
                if check_channel:
                    if (channel == r.channel.strip().lower() or \
                       r.channel == _("All") or r.channel == ""):
                        return True
                else:
                    return True
        return False

    def add(self, reminder):
        if not reminder in self.reminders:
            self.reminders.append(reminder)
            if self.config.debug:
                print("Added reminder: %s" % reminder)
            return True
        return False

    def remove(self, reminder):
        if reminder in self.reminders:
            self.reminders.remove(reminder)
            if self.config.debug:
                print("Removed reminder: %s" % reminder)
            return True
        return False

    def update(self, current_reminder, new_reminder):
        if current_reminder in self.reminders:
            index = self.reminders.index(current_reminder)
            self.reminders.remove(current_reminder)
            self.reminders.insert(index, new_reminder)
            if self.config.debug:
                print("Updated reminder: %s" % new_reminder)
            return True
        return False

    def __save_in_thread(self):
        file = open(self.file, 'w')
        cPickle.dump(self, file, cPickle.HIGHEST_PROTOCOL)
        file.close()
        if self.config.debug:
            print("Saved reminders file: %s" % self.file)

    def save(self):
        thread.start_new_thread(self.__save_in_thread, ())

    def load(self):
        file = open(self.file)
        self = cPickle.load(file)
        file.close()
        if self.config.debug:
            print("Loaded reminders from file: %s" % self.file)
        return self

class Reminder:
    def __init__(self, *args):
        if len(args) == 1:
            program = args[0]
            notify_time = (program.get_time_until_start().seconds / 60)
            self.__init__(program.title, program.channel.markup_escaped_name,
                          notify_time)
        elif len(args) == 2 or len(args) == 3:
            self.program = args[0]
            self.channel = args[1]
            if len(args) == 3:
                self.notify_time = args[2]
            else:
                self.notify_time = 5

    def __cmp__(self, other):
        if self.program.strip().lower() == other.program.strip().lower() and \
           self.channel.strip().lower() == other.channel.strip().lower() and \
           self.notify_time == other.notify_time:
               return 0
        return -1

    def __str__(self):
        return "Program: %s on Channel: %s " % (self.program, self.channel)

# vim: set sw=4 et sts=4 tw=79 fo+=l:
