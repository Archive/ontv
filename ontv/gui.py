# -*- coding: utf-8 -*-

# Copyright (C) 2004-2010 Johan Svedberg <johan@svedberg.com>

# This file is part of OnTV.

# OnTV is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# OnTV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with OnTV; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

import os.path
from gettext import gettext as _

import gtk
import gtk.gdk

from dialogs import ProgramDialog
from reminders import Reminder
from ontv import NAME, DATA_DIR, LOCALE_DIR, IMAGES_DIR

about_dialog_ui_file = os.path.join(DATA_DIR, "about_dialog.ui")
channel_dialog_ui_file = os.path.join(DATA_DIR, "channel_dialog.ui")
program_dialog_ui_file = os.path.join(DATA_DIR, "program_dialog.ui")
search_dialog_ui_file = os.path.join(DATA_DIR, "search_dialog.ui")
preferences_dialog_ui_file = os.path.join(DATA_DIR, "preferences_dialog.ui")
program_window_ui_file = os.path.join(DATA_DIR, "program_window.ui")
icon_theme = gtk.icon_theme_get_default()

def get_icon_list(sizes):
    icon_list = []
    for size in sizes:
        icon_list.append(load_icon(NAME.lower(), size, size))
    return icon_list

def load_icon(icon, width=48, height=48):
    pixbuf = None
    if icon != None and icon != "":
        try:
            icon_file = os.path.join(IMAGES_DIR, icon)
            if os.path.exists(icon_file):
                pixbuf = gtk.gdk.pixbuf_new_from_file_at_size(icon_file, width,
                                                              height)
            elif icon.startswith("/"):
                pixbuf = gtk.gdk.pixbuf_new_from_file_at_size(icon, width,
                                                              height)
            else:
                pixbuf = icon_theme.load_icon(os.path.splitext(icon)[0], width,
                                              gtk.ICON_LOOKUP_USE_BUILTIN)
        except Exception, msg1:
            try:
                pixbuf = icon_theme.load_icon(icon, width,
                                              gtk.ICON_LOOKUP_USE_BUILTIN)
            except Exception, msg2:
                print 'Error:load_icon:Icon Load Error:%s (or %s)' % (msg1, msg2
)

    return pixbuf

def has_selection(treeview):
    selection = treeview.get_selection()
    (model, iter) = selection.get_selected()
    if iter:
        return True
    return False

def get_selected_value(treeview, col=0):
    selection = treeview.get_selection()
    (model, iter) = selection.get_selected()
    if iter:
        return model.get_value(iter, col)
    return None

def queue_resize(treeview):
    columns = treeview.get_columns()
    for column in columns:
        column.queue_resize()

def set_active_from_string(combobox, string):
    model = combobox.get_model()
    iter = model.get_iter_first()
    for row in model:
        if row[0] == string:
            combobox.set_active_iter(iter)
            return
        iter = model.iter_next(iter)
    combobox.prepend_text(string)
    set_active_from_string(combobox, string)

class ProgramContextMenu(gtk.Menu):
    def __init__(self, get_program, reminders, reminders_model):
        gtk.Menu.__init__(self)

        self.get_program = get_program
        self.reminders = reminders
        self.reminders_model = reminders_model

        self.details_imi = gtk.ImageMenuItem(gtk.STOCK_INFO)
        self.details_imi.child.set_markup_with_mnemonic(_("_Details"))
        self.details_imi.show()
        self.append(self.details_imi)

        self.add_reminder_imi = gtk.ImageMenuItem("gnome-stock-timer")
        self.add_reminder_imi.child.set_markup_with_mnemonic(_("_Add reminder"))
        self.add_reminder_imi.show()
        self.append(self.add_reminder_imi)

        self.details_imi.connect("activate", self.__details_imi_activate)
        self.add_reminder_imi.connect("activate",
                                      self.__add_reminder_imi_activate)

    def __details_imi_activate(self, menuitem):
        program = self.get_program()
        pd = ProgramDialog(program)
        pd.show()

    def __add_reminder_imi_activate(self, menuitem):
        program = self.get_program()
        reminder = Reminder(program.title, program.channel.markup_escaped_name)
        if self.reminders.add(reminder):
            self.reminders_model.append([reminder])
            self.reminders.save()
            self.add_reminder_imi.set_sensitive(False)

class FileChooserDialog(gtk.FileChooserDialog):
    def __init__(self, title, filters=None):
        gtk.FileChooserDialog.__init__(self, title=title,
                                            action=gtk.FILE_CHOOSER_ACTION_OPEN,
                                            buttons=(gtk.STOCK_CANCEL,
                                            gtk.RESPONSE_CANCEL,
                                            gtk.STOCK_OPEN, gtk.RESPONSE_OK))
        if filters:
            for filter in filters:
                self.add_filter(filter)
        all_filter = gtk.FileFilter()
        all_filter.set_name(_("All files"))
        all_filter.add_pattern("*")
        self.add_filter(all_filter)

class ErrorDialog(gtk.MessageDialog):
    def __init__(self, primary_msg, secondary_msg):
        gtk.MessageDialog.__init__(self, parent=None, flags=gtk.DIALOG_MODAL|gtk. DIALOG_DESTROY_WITH_PARENT, type=gtk.MESSAGE_ERROR, buttons=gtk.BUTTONS_OK, message_format=primary_msg)
        self.format_secondary_text(secondary_msg)
        self.set_title(_("Error"))

# vim: set sw=4 et sts=4 tw=79 fo+=l:
