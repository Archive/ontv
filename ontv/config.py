# -*- coding: utf-8 -*-

# Copyright (C) 2004-2010 Johan Svedberg <johan@svedberg.com>

# This file is part of OnTV.

# OnTV is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# OnTV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with OnTV; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

import os.path
import sys
from gettext import gettext as _

import gconf

import gui

DIR_ONTV = "/apps/ontv"

KEY_GRABBER_COMMAND             = "/general/grabber_command"
KEY_XMLTV_FILE                  = "/general/xmltv_file"
KEY_ENABLE_HOTKEYS              = "/general/enable_hotkeys"
KEY_SHOW_WINDOW_HOTKEY          = "/general/show_window_hotkey"
KEY_SHOW_SEARCH_PROGRAM_HOTKEY  = "/general/show_search_program_hotkey"
KEY_DISPLAY_CURRENT_PROGRAMS    = "/general/display_current_programs"
KEY_DISPLAY_UPCOMING_PROGRAMS   = "/general/display_upcoming_programs"
KEY_UPCOMING_PROGRAMS_BELOW     = "/general/upcoming_programs_below"

FUNCTION_SUFFIXES = {KEY_GRABBER_COMMAND:             'string',
                     KEY_XMLTV_FILE:                  'string',
                     KEY_ENABLE_HOTKEYS:              'bool',
                     KEY_SHOW_WINDOW_HOTKEY:          'string',
                     KEY_SHOW_SEARCH_PROGRAM_HOTKEY:  'string',
                     KEY_DISPLAY_CURRENT_PROGRAMS:    'bool',
                     KEY_DISPLAY_UPCOMING_PROGRAMS:   'bool',
                     KEY_UPCOMING_PROGRAMS_BELOW:     'bool'}

XDG_CONFIG_DIR = os.getenv("XDG_CONFIG_HOME") or "~/.config"

class Configuration(object):
    """Singleton representing the configuration"""

    instance = None

    def __new__(type, *args):
        if Configuration.instance is None:
            Configuration.instance = object.__new__(type)
            Configuration.instance.__init(*args)
        return Configuration.instance

    def __init(self, *args):
        self.base_dir = os.path.expanduser(os.path.join(XDG_CONFIG_DIR,
            "ontv"))
        self.logos_dir = os.path.join(self.base_dir, "logos")
        if not os.path.exists(self.logos_dir):
            os.makedirs(self.logos_dir)

        self.client = gconf.client_get_default()
        if self.client.dir_exists(DIR_ONTV):
            self.client.add_dir(DIR_ONTV, gconf.CLIENT_PRELOAD_RECURSIVE)
            self.debug = args[0] or os.getenv("ONTV_DEBUG")
            self.__init_option_cache()
        else:
            ed = gui.ErrorDialog(_("Could not find configuration directory in GConf"), _("Please make sure that ontv.schemas was correctly installed."))
            ed.run()
            sys.exit(1)

    def __init_option_cache(self):
        self.option_cache = {}
        for key in FUNCTION_SUFFIXES.keys():
            self.option_cache[key] = getattr(self, 'get_' +
                                             os.path.basename(key))(False)

    def add_notify(self, key, callback):
        self.client.notify_add(DIR_ONTV + key, callback)

    def __get_option(self, option, type=None):
        if self.debug:
            print '[GConf get]: %s%s' % (DIR_ONTV, option)
        if type:
            return getattr(self.client, 'get_' +
                           FUNCTION_SUFFIXES[option])(DIR_ONTV + option, type)
        else:
            return getattr(self.client, 'get_' +
                           FUNCTION_SUFFIXES[option])(DIR_ONTV + option)

    def __set_option(self, option, value, type=None):
        if self.debug:
            print '[GConf set]: %s%s=%s' % (DIR_ONTV, option, str(value))
        if type:
            getattr(self.client, 'set_' +
                    FUNCTION_SUFFIXES[option])(DIR_ONTV + option, type, value)
            self.option_cache[option] = value
        else:
            getattr(self.client, 'set_' +
                    FUNCTION_SUFFIXES[option])(DIR_ONTV + option, value)
            self.option_cache[option] = value

    def get_grabber_command(self, use_cache=True):
        if use_cache:
            return self.option_cache[KEY_GRABBER_COMMAND]
        else:
            return self.__get_option(KEY_GRABBER_COMMAND)

    def set_grabber_command(self, grabber_command):
        self.__set_option(KEY_GRABBER_COMMAND, grabber_command)

    grabber_command = property(get_grabber_command, set_grabber_command)

    def get_xmltv_file(self, use_cache=True):
        if use_cache:
            return self.option_cache[KEY_XMLTV_FILE]
        else:
            return self.__get_option(KEY_XMLTV_FILE)

    def set_xmltv_file(self, xmltv_file):
        self.__set_option(KEY_XMLTV_FILE, xmltv_file)

    xmltv_file = property(get_xmltv_file, set_xmltv_file)

    def get_enable_hotkeys(self, use_cache=True):
        if use_cache:
            return self.option_cache[KEY_ENABLE_HOTKEYS]
        else:
            return self.__get_option(KEY_ENABLE_HOTKEYS)

    def set_enable_hotkeys(self, enable_hotkeys):
        self.__set_option(KEY_ENABLE_HOTKEYS, enable_hotkeys)

    enable_hotkeys = property(get_enable_hotkeys, set_enable_hotkeys)

    def get_show_window_hotkey(self, use_cache=True):
        if use_cache:
            return self.option_cache[KEY_SHOW_WINDOW_HOTKEY]
        else:
            return self.__get_option(KEY_SHOW_WINDOW_HOTKEY)

    def set_show_window_hotkey(self, show_window_hotkey):
        self.__set_option(KEY_SHOW_WINDOW_HOTKEY,
                          show_window_hotkey)

    show_window_hotkey = property(get_show_window_hotkey,
                                  set_show_window_hotkey)

    def get_show_search_program_hotkey(self, use_cache=True):
        if use_cache:
            return self.option_cache[KEY_SHOW_SEARCH_PROGRAM_HOTKEY]
        else:
            return self.__get_option(KEY_SHOW_SEARCH_PROGRAM_HOTKEY)

    def set_show_search_program_hotkey(self, show_search_program_hotkey):
        self.__set_option(KEY_SHOW_SEARCH_PROGRAM_HOTKEY,
                          show_search_program_hotkey)

    show_search_program_hotkey = property(get_show_search_program_hotkey,
                                          set_show_search_program_hotkey)
    def get_display_current_programs(self, use_cache=True):
        if use_cache:
            return self.option_cache[KEY_DISPLAY_CURRENT_PROGRAMS]
        else:
            return self.__get_option(KEY_DISPLAY_CURRENT_PROGRAMS)

    def set_display_current_programs(self, display_current_programs):
        self.__set_option(KEY_DISPLAY_CURRENT_PROGRAMS,
                          display_current_programs)

    display_current_programs = property(get_display_current_programs,
                                        set_display_current_programs)

    def get_display_upcoming_programs(self, use_cache=True):
        if use_cache:
            return self.option_cache[KEY_DISPLAY_UPCOMING_PROGRAMS]
        else:
            return self.__get_option(KEY_DISPLAY_UPCOMING_PROGRAMS)

    def set_display_upcoming_programs(self, display_upcoming_programs):
        self.__set_option(KEY_DISPLAY_UPCOMING_PROGRAMS,
                          display_upcoming_programs)

    display_upcoming_programs = property(get_display_upcoming_programs,
                                         set_display_upcoming_programs)

    def get_upcoming_programs_below(self, use_cache=True):
        if use_cache:
            return self.option_cache[KEY_UPCOMING_PROGRAMS_BELOW]
        else:
            return self.__get_option(KEY_UPCOMING_PROGRAMS_BELOW)

    def set_upcoming_programs_below(self, upcoming_programs_below):
        self.__set_option(KEY_UPCOMING_PROGRAMS_BELOW,
                          upcoming_programs_below)

    upcoming_programs_below = property(get_upcoming_programs_below,
                                       set_upcoming_programs_below)

# vim: set sw=4 et sts=4 tw=79 fo+=l:
