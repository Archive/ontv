# -*- coding: utf-8 -*-

# Copyright (C) 2004-2010 Johan Svedberg <johan@svedberg.com>

# This file is part of OnTV.

# OnTV is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# OnTV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with OnTV; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

import os.path
import re

def is_in_path(file):
    paths = ["/usr/bin", "/usr/local/bin"]
    for path_dir in paths:
        if os.path.isfile(os.path.join(path_dir, file)):
            return True
    return False


def try_int(i):
     # Convert to an interger if we can
    try: return int(i)
    except: return i

def natsort_key(s):
    # Used to get a tuple sorted
    return map(try_int, re.findall(r'(\d+|\D+)', s))

def natcmp(a, b):
    # Natural comparison - case sensitive
    return cmp(natsort_key(a), natsort_key(b))

def natcasecmp(a, b):
    # Natural comparison - case insensitive
    return natcmp(a.lower(), b.lower())

def natsort(seq, cmp=natcmp):
    # Inplace natural sorting
    seq.sort(cmp)
    
def natsorted(seq, cmp=natcmp):
    # Returns a copy of seq, naturally sorted
    import copy
    temp = copy.copy(seq)
    natsort(temp, cmp)
    return temp
