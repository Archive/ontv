# -*- coding: utf-8 -*-

# Copyright (C) 2004-2010 Johan Svedberg <johan@svedberg.com>

# This file is part of OnTV.

# OnTV is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# OnTV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with OnTV; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

import sys
from gettext import gettext as _

import gobject

from dialogs import ProgramDialog

from ontv import NAME
from gui import ErrorDialog
try:
    import pynotify
except ImportError, ie:
    if str(ie) == "No module named pynotify":
        ed = ErrorDialog(_("Error while importing pynotify module"),
                         _("Could not find python-notify."))
        ed.run()
        sys.exit(1)

TIMEOUT = 60

class Notification:
    def __init__(self, program, get_readable_time):
        self.program = program
        self.get_readable_time = get_readable_time
        pynotify.init(NAME)

        #self.__update_id = gobject.timeout_add(TIMEOUT, self.__update)

        self.__show()

    def __update(self):
        self.notification.update(self.__get_summary(), self.__get_body())
        timeout = self.__get_timeout()
        if timeout <= 1:
            return False
        self.notification.set_timeout(timeout)
        self.notification.show()
        return True

    def __get_summary(self):
        summary = "%s %s" % (self.program.start_time,
                             self.program.markup_escaped_title)
        if self.program.channel.logo is None:
            summary += "\n" + self.program.channel.markup_escaped_name
        return summary

    def __get_body(self):
        return _("%s until the program begins.") % \
               self.get_readable_time(self.program.get_time_until_start())

    def __get_timeout(self):
        return self.program.get_time_until_start().seconds

    def __show(self):
        self.notification = pynotify.Notification(self.__get_summary(),
                                                  self.__get_body())
        if not self.program.channel.logo is None:
            self.notification.set_icon_from_pixbuf(self.program.channel.logo)
        self.notification.show()

    def __action_invoked(self, notification, action):
        if action == "details":
            pd = ProgramDialog(self.program)
            pd.show()

    def __closed(self, notification):
        gobject.source_remove(self.__update_id)
        notification.close()

# vim: set sw=4 et sts=4 tw=79 fo+=l:
