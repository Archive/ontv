# -*- coding: utf-8 -*-

# Copyright (C) 2004-2010 Johan Svedberg <johan@svedberg.com>

# This file is part of OnTV.

# OnTV is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# OnTV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with OnTV; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

import os.path
from gettext import gettext as _

import gtk
import gnomeapplet

import gui
from ontv import NAME, UI_DIR
from ontv_core import OnTVCore

class OnTVApplet(object):
    """Singleton representing the applet"""

    instance = None

    def __new__(type, *args):
        if OnTVApplet.instance is None:
            OnTVApplet.instance = object.__new__(type)
            OnTVApplet.instance.__init(*args)
        return OnTVApplet.instance

    def __init(self, *args):
        self.applet     = args[0]
        configure       = args[1]
        debug           = args[2]

        self.ontv_core = OnTVCore(configure, debug, self.cb_status)

        self.applet.set_applet_flags(gnomeapplet.EXPAND_MINOR)

        ui_file = os.path.join(UI_DIR, "GNOME_OnTVApplet.xml")
        verb_list = [("Update TV listings", self.__update_listings),
                     ("Preferences", self.__show_pd),
                     ("Search Program", self.__show_sd),
                     ("About", self.__run_ad)]
        self.applet.setup_menu_from_file(None, ui_file, NAME, verb_list)

        self.applet.connect("button-press-event", self.__button_press)

        self.image = gtk.Image()
        size = self.applet.get_size()
        pixbuf = gui.load_icon("ontv", size, size)
        if pixbuf.get_width() != size or pixbuf.get_height() != size:
            pixbuf = pixbuf.scale_simple(size-4, size-4,
                                         gtk.gdk.INTERP_BILINEAR)
        self.image.set_from_pixbuf(pixbuf)

        self.applet.add(self.image)

        self.applet.connect("change-size", self.__change_size)
        self.applet.connect("change-background", self.__change_background)

        self.applet.show_all()

    def __update_listings(self, uicomponent=None, verb=None):
        self.ontv_core.update_listings()

    def __run_ad(self, uicomponent=None, verb=None):
        self.ontv_core.show_about_dialog()

    def __show_sd(self, uicomponent, verb):
        self.ontv_core.show_search_dialog()

    def __show_pd(self, uicomponent, verb):
        self.ontv_core.show_preferences_dialog()

    def run(self):
        self.ontv_core.run()
        self.ontv_core.set_program_window_position(self.get_docking_data())

    def __button_press(self, button, event):
        if event.type == gtk.gdk.BUTTON_PRESS and event.button == 1:
            self.__press_button()

    def __press_button(self):
        self.ontv_core.set_program_window_position(self.get_docking_data())
        if self.ontv_core.toggle_program_window():
            self.applet.set_state(gtk.STATE_SELECTED)
        else:
            self.applet.set_state(gtk.STATE_NORMAL)

    def __change_size(self, widget, size):
        pixbuf = gui.load_icon("ontv", size, size)
        if pixbuf.get_width() != size or pixbuf.get_height() != size:
            pixbuf = pixbuf.scale_simple(size-4, size-4,
                                         gtk.gdk.INTERP_BILINEAR)
        self.image.set_from_pixbuf(pixbuf)

    def __change_background(self, widget, background, colour, pixmap):
        widgets = (self.applet, self.image)
        if background == gnomeapplet.NO_BACKGROUND or \
           background == gnomeapplet.PIXMAP_BACKGROUND:
            for widget in widgets:
                copy = widget.get_style().copy()
                copy.bg_pixmap[gtk.STATE_NORMAL] = pixmap
                copy.bg_pixmap[gtk.STATE_INSENSITIVE]  = pixmap
                widget.set_style(copy)
        elif background == gnomeapplet.COLOR_BACKGROUND:
            for widget in widgets:
                widget.modify_bg(gtk.STATE_NORMAL, colour)

    def set_tooltip(self, text):
        self.applet.set_tooltip_text(text)

    def cb_status(self, msg):
        if msg:
            self.set_tooltip(msg)

    def __kb_activated(self, kb, name, time):
        if name == "show-window-hotkey":
            self.__press_button()
        elif name == "show-search-program-hotkey":
            self.sd.present(time)

    def get_docking_data(self):
        (w, h) = self.ontv_core.get_program_window_size()
        middle = False
        self.applet.realize()
        (x, y) = self.applet.window.get_origin()

        button_w = self.applet.allocation.width
        button_h = self.applet.allocation.height

        screen = self.applet.get_screen()

        found_monitor = False
        n = screen.get_n_monitors()
        for i in range(0, n):
            monitor = screen.get_monitor_geometry(i)
            if (x >= monitor.x and x <= monitor.x + monitor.width and
                y >= monitor.y and y <= monitor.y + monitor.height):
                    found_monitor = True
                    break

        if not found_monitor:
            screen_width = screen.get_width()
            monitor = gtk.gdk.Rectangle(0, 0, screen_width, screen_width)

        orient = self.applet.get_orient()

        if orient == gnomeapplet.ORIENT_RIGHT:
            x += button_w

            if ((y + h) > monitor.y + monitor.height):
                y -= (y + h) - (monitor.y + monitor.height)

            if middle:
                x -= button_w/2
                y += button_h/2

            if ((y + h) > (monitor.height / 2)):
                gravity = gtk.gdk.GRAVITY_SOUTH_WEST
            else:
                gravity = gtk.gdk.GRAVITY_NORTH_WEST
        elif orient == gnomeapplet.ORIENT_LEFT:
            x -= w

            if ((y + h) > monitor.y + monitor.height):
                y -= (y + h) - (monitor.y + monitor.height)

            if middle:
                x += w/2
                y += button_h/2

            if ((y + h) > (monitor.height / 2)):
                gravity = gtk.gdk.GRAVITY_SOUTH_EAST
            else:
                gravity = gtk.gdk.GRAVITY_NORTH_EAST
        elif orient == gnomeapplet.ORIENT_DOWN:
            y += button_h

            if ((x + w) > monitor.x + monitor.width):
                x -= (x + w) - (monitor.x + monitor.width)

            if middle:
                x += button_w/2
                y -= button_h/2

            gravity = gtk.gdk.GRAVITY_NORTH_WEST
        elif orient == gnomeapplet.ORIENT_UP:
            y -= h

            if ((x + w) > monitor.x + monitor.width):
                x -= (x + w) - (monitor.x + monitor.width)

            if middle:
                x += button_w/2
                y += h/2

            gravity = gtk.gdk.GRAVITY_SOUTH_WEST
        return (x, y, gravity)

# vim: set sw=4 et sts=4 tw=79 fo+=l:
