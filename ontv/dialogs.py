# -*- coding: utf-8 -*-

# Copyright (C) 2004-2010 Johan Svedberg <johan@svedberg.com>

# This file is part of OnTV.

# OnTV is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# OnTV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with OnTV; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

import struct
import os

import gtk
import gconf
import gobject
import pango

import config
import gui
import utils
from reminders import Reminder
from ontv import NAME, VERSION, LOCALE_DIR

import locale
locale.setlocale(locale.LC_ALL, '')
locale.bindtextdomain(NAME.lower(), LOCALE_DIR)
import gettext
gettext.bindtextdomain(NAME.lower(), LOCALE_DIR)
gettext.textdomain(NAME.lower())
_ = gettext.gettext

COL_PATH   = 0
COL_PIXBUF = 1
COL_ISDIR  = 2
COL_TITLE  = 3

class AboutDialog:
    def __init__(self):
        builder = gtk.Builder()
        builder.add_from_file(gui.about_dialog_ui_file)
        ad = builder.get_object("about_dialog")
        ad.set_name(NAME)
        ad.set_version(VERSION)
        ad.connect("response", lambda d, r: d.destroy())
        ad.show()

class ChannelDialog:
    def __init__(self, channel, pd):
        self.channel = channel
        self.pd = pd
        builder = gtk.Builder()
        builder.set_translation_domain(NAME.lower())
        builder.add_from_file(gui.channel_dialog_ui_file)
        builder.connect_signals(self)
        self.dialog = builder.get_object("channel_dialog")
        self.iconview = builder.get_object("channel_iconview")
        self.store = builder.get_object("channel_icon_store")
        self.store.set_sort_column_id(COL_PATH, gtk.SORT_ASCENDING)

        theme = gtk.icon_theme_get_default()
        self.dirIcon = theme.load_icon(gtk.STOCK_OPEN, 48, 0)
        self.upIcon = theme.load_icon(gtk.STOCK_GO_UP, 48, 0)
        self.dialog.set_size_request(800,500)
        self.iconview.set_pixbuf_column(COL_PIXBUF)
        self.iconview.set_text_column(COL_TITLE)
        self.okbutton = builder.get_object("channel_okbutton")
        if self.channel.logo_file != "":
            self.curdir = os.path.dirname(self.channel.logo_file)
        else:
            self.curdir = os.getcwd()

        self.dialog.set_title(_("Properties for channel %s") % channel.name)

        self.update_model()

    def update_model(self):
        self.store.clear()
        if self.curdir == None:
            return

        self.store.append([os.path.dirname(self.curdir), self.upIcon, True, ".."])

        for f in os.listdir(self.curdir):
            file = os.path.join(self.curdir,f)
            if os.path.isdir(file):
                self.store.append([file,self.dirIcon,True,f])
            elif gtk.gdk.pixbuf_get_file_info(file):
                fileIcon = gtk.gdk.pixbuf_new_from_file(file)
                title = os.path.splitext(f)[0]
                self.store.append([file,fileIcon,False,title])
        
    def on_channel_iconview_item_activated(self,store,item):
        model = store.get_model()
        if model[item][COL_ISDIR]:
            self.curdir = os.path.join(self.curdir,model[item][COL_PATH])
            self.update_model()
            

    def on_channel_iconview_selection_changed(self, iconview):
        item = iconview.get_selected_items()
        model = iconview.get_model()
        valid_icon = False
        if len(item) > 0:
            if not model[item[0]][COL_ISDIR]:
                valid_icon = True
        self.okbutton.set_sensitive(valid_icon)

    def on_channel_cancelbutton_clicked(self, button):
        self.dialog.destroy()

    def on_channel_okbutton_clicked(self, button):
        item = self.iconview.get_selected_items()
        model = self.iconview.get_model()
        icon_file = model[item[0]][COL_PATH]

        if not icon_file:
            self.channel.logo_file = ""
            self.channel.logo = self.channel.logo_small = None
        else:
            self.channel.logo_file = icon_file
            self.channel.set_logo(self.channel.logo_file)
        self.channel.custom_logo = True

        logo_column = self.pd.channels_treeview.get_column(1)
        logo_column.queue_resize()
        self.pd.pw.update()
        self.pd.listings.save()
        self.dialog.destroy()

class PreferencesDialog:
    def __init__(self, config, xmltvfile, reminders, pw, sd):
        self.config = config
        self.xmltvfile = xmltvfile
        self.reminders = reminders
        self.pw = pw
        self.sd = sd

        self.xmltvfile.connect("loading", self.__xmltvfile_loading)
        self.xmltvfile.connect("loading-done", self.__xmltvfile_loading_done)
        self.xmltvfile.connect("loaded-channel",
                               self.__xmltvfile_loaded_channel)
        self.xmltvfile.connect("downloading-logo", self.__downloading_logo)
        self.xmltvfile.connect("downloading-logo-done",
                               self.__downloading_logo_done)

        self.__get_widgets()

        self.__make_reminders_treeview()
        #self.sd.create_search_treeview_menu(self.reminders_treeview.get_model())
        self.pw.set_reminders_model(self.reminders_treeview.get_model())

        self.channels_combobox_model = gtk.ListStore(str)
        self.channels_combobox_model.set_sort_func(0, self.__channels_model_sort_func, False)
        self.channels_combobox_model.set_sort_column_id(0, gtk.SORT_ASCENDING)
        self.channels_comboboxentry.set_model(self.channels_combobox_model)
        self.channels_comboboxentry.set_text_column(0)
        self.channels_comboboxentry.child.set_text(_("All"))
        self.sd.channels_combobox.set_model(self.channels_combobox_model)

        self.__connect_widgets()

        self.__add_config_notifications()

        self.grabber_command_entry.set_text(self.config.grabber_command)
        self.output_file_entry.set_text(self.config.xmltv_file)
        self.current_programs_checkbutton.set_active(self.config.display_current_programs)
        self.upcoming_programs_checkbutton.set_active(self.config.display_upcoming_programs)
        if self.config.upcoming_programs_below:
            self.upcoming_programs_below_radiobutton.set_active(True)
        else:
            self.upcoming_programs_right_radiobutton.set_active(True)

        self.__auto_reload_id = None

    def __get_widgets(self):
        builder = gtk.Builder()
        builder.set_translation_domain(NAME.lower())
        builder.add_from_file(gui.preferences_dialog_ui_file)
        builder.connect_signals(self)

        self.dialog = builder.get_object("preferences_dialog")

        # General tab
        self.grabber_command_entry = builder.get_object("grabber_command_entry")
        self.output_file_entry = builder.get_object("output_file_entry")
        self.current_programs_checkbutton = builder.get_object("current_programs_checkbutton")
        self.upcoming_programs_checkbutton = builder.get_object("upcoming_programs_checkbutton")
        self.position_hbox = builder.get_object("position_hbox")
        self.upcoming_programs_below_radiobutton = builder.get_object("upcoming_programs_below_radiobutton")
        self.upcoming_programs_right_radiobutton = builder.get_object("upcoming_programs_right_radiobutton")

        # Channels tab
        self.channels_treeview = builder.get_object("channels_treeview")
        self.selected_channels_treeview = builder.get_object("selected_channels_treeview")
        self.channels_model = builder.get_object("channels_model")
        self.unselected_channels_filter = builder.get_object("channels_unselected_filter")
        self.selected_channels_filter = builder.get_object("channels_selected_filter")
        builder.get_object("col_ch_logo").set_cell_data_func(builder.get_object("cr_logo"),
                                                self.__crpixbuf_cell_data_func)
        builder.get_object("col_ch_name").set_cell_data_func(builder.get_object("cr_name"),
                                                self.__crtext_cell_data_func)
        builder.get_object("col_ch_sel_logo").set_cell_data_func(builder.get_object("cr_ch_sel_logo"),
                                                self.__crpixbuf_cell_sel_data_func)
        builder.get_object("col_ch_sel_name").set_cell_data_func(builder.get_object("cr_ch_sel_name"),
                                                self.__crtext_cell_sel_data_func)
        self.unselected_channels_filter.set_visible_func(self.filter)
        self.selected_channels_model = builder.get_object("channels_selected_model")
        self.channel_properties_button = builder.get_object("channel_properties_button")
        self.button_add_channel = builder.get_object("button_add_channel")
        self.button_remove_channel = builder.get_object("button_remove_channel")
        self.button_move_channel_up = builder.get_object("button_move_channel_up")
        self.button_move_channel_down = builder.get_object("button_move_channel_down")

        # Reminders tab
        self.reminders_treeview = builder.get_object("reminders_treeview")
        self.program_entry = builder.get_object("program_entry")
        self.channels_comboboxentry = builder.get_object("channels_comboboxentry")
        self.notify_spinbutton = builder.get_object("notify_spinbutton")
        self.add_reminder_button = builder.get_object("add_reminder_button")
        self.update_reminder_button = builder.get_object("update_reminder_button")
        self.remove_reminder_button = builder.get_object("remove_reminder_button")

    def __channels_model_sort_func(self, model, iter1, iter2, object=True):
        channel = model.get_value(iter1, 0)
        other_channel = model.get_value(iter2, 0)
        if object:
            return utils.natcmp(channel.name.lower(), other_channel.name.lower())
        return utils.natcmp(channel.lower(), other_channel.lower())

    def filter(self, model, iter):
        if iter:
            channel = model.get_value(iter, 0)
            if channel:
                return not channel.selected

    def on_channels_treeview_toggled(self, model, path):
        channel = model[path][0]
        channel.selected = not channel.selected
        self.listings.set_channel_selected(channel.name, channel.selected)

        model.row_changed(path, model.get_iter(path))

        self.pw.update()
        self.listings.save()

    def __channels_treeview_search_equal(self, model, column, key, iter):
        channel = model.get_value(iter, 0)
        return key.lower() not in channel.name.lower()

    def __crpixbuf_cell_data_func(self, column, cell, model, iter):
        channel = model.get_value(iter, 0)
        cell.props.pixbuf = channel.logo

    def __crtext_cell_data_func(self, column, cell, model, iter):
        channel = model.get_value(iter, 0)
        markup = "<b>%s</b>"
        cell.props.markup = markup % channel.markup_escaped_name

    def __crpixbuf_cell_sel_data_func(self, column, cell, model, iter):
        channel_name = model.get_value(iter, 0)
        channel = self.listings.get_channel(channel_name)
        cell.props.pixbuf = channel.logo

    def __crtext_cell_sel_data_func(self, column, cell, model, iter):
        channel_name = model.get_value(iter, 0)
        channel = self.listings.get_channel(channel_name)
        markup = "<b>%s</b>"
        cell.props.markup = markup % channel.markup_escaped_name

    def __make_reminders_treeview(self):
        reminders_model = gtk.ListStore(object)
        self.reminders_treeview.set_model(reminders_model)

        program_crt = gtk.CellRendererText()
        program_crt.props.xpad = 3
        program_crt.props.ypad = 3
        program_column = gtk.TreeViewColumn(_("Program"), program_crt)
        program_column.set_cell_data_func(program_crt,
                                          self.__program_crt_cell_data_func)
        self.reminders_treeview.append_column(program_column)

        channel_crt = gtk.CellRendererText()
        channel_crt.props.xpad = 3
        channel_crt.props.ypad = 3
        channel_column = gtk.TreeViewColumn(_("Channel"), channel_crt)
        channel_column.set_cell_data_func(channel_crt,
                                          self.__channel_crt_cell_data_func)
        self.reminders_treeview.append_column(channel_column)

        notify_crt = gtk.CellRendererText()
        notify_crt.props.xpad = 3
        notify_crt.props.ypad = 3
        notify_column = gtk.TreeViewColumn(_("Notification time"), notify_crt)
        notify_column.set_cell_data_func(notify_crt,
                                         self.__notify_crt_cell_data_func)
        self.reminders_treeview.append_column(notify_column)

        for reminder in self.reminders.reminders:
            reminders_model.append([reminder])

    def __program_crt_cell_data_func(self, column, cell, model, iter):
        reminder = model.get_value(iter, 0)
        cell.props.text = reminder.program

    def __channel_crt_cell_data_func(self, column, cell, model, iter):
        reminder = model.get_value(iter, 0)
        cell.props.markup = reminder.channel

    def __notify_crt_cell_data_func(self, column, cell, model, iter):
        reminder = model.get_value(iter, 0)
        cell.props.text = reminder.notify_time

    def __connect_widgets(self):
        self.dialog.connect("delete-event", self.__dialog_delete)
        self.dialog.connect("response", self.__dialog_response)

        #Channels tab
        selection = self.channels_treeview.get_selection()
        selection.connect("changed", self.__channels_treeview_selection_changed)

        selection = self.selected_channels_treeview.get_selection()
        selection.connect("changed", self.__selected_channels_treeview_selection_changed)

        # Reminders tab
        self.reminders_treeview.connect("key-press-event", self.__reminders_treeview_key_press_event)
        selection = self.reminders_treeview.get_selection()
        selection.connect("changed",
                          self.__reminders_treeview_selection_changed)
        self.program_entry.connect("changed", self.__program_entry_changed)
        self.program_entry.connect("activate",
                                   self.__update_reminder_button_clicked)
        self.channels_comboboxentry.connect("changed", self.__channels_comboboxentry_changed)
        self.channels_comboboxentry.child.connect("activate", self.__update_reminder_button_clicked)
        self.notify_spinbutton.connect("value_changed",
                                       self.__notify_spinbutton_value_changed)
        self.notify_spinbutton.connect("activate",
                                       self.__update_reminder_button_clicked)
        self.add_reminder_button.connect("clicked",
                                         self.__add_reminder_button_clicked)
        self.update_reminder_button.connect("clicked", self.__update_reminder_button_clicked)
        self.remove_reminder_button.connect("clicked", self.__remove_reminder_button_clicked)

    def __dialog_response(self, dialog, response):
        self.dialog.hide()

    def __dialog_delete(self, dialog, event):
        return True

    def __selected_channels_treeview_selection_changed(self, selection):
        (model, iter) = selection.get_selected()
        if iter:
            self.channels_treeview.get_selection().unselect_all()
            self.update_buttons(selection, True)
        else:
            self.update_buttons(None, True)

    def __channels_treeview_selection_changed(self, selection):
        (model, iter) = selection.get_selected()
        if iter:
            self.selected_channels_treeview.get_selection().unselect_all()
            self.update_buttons(selection, False)
        else:
            self.update_buttons(None, False)

    def update_buttons(self, selection, selected):
        if selection:
            self.channel_properties_button.set_sensitive(True)
            (model, iter) = selection.get_selected()

            self.button_remove_channel.set_sensitive(selected)
            self.button_add_channel.set_sensitive(not selected)

            if selected:
                channel_name = model.get_value(iter, 0)
                self.selected_channel = self.listings.get_channel(channel_name)

                row, = model.get_path(iter)
                if row == 0:
                    self.button_move_channel_up.set_sensitive(False)
                else:
                    self.button_move_channel_up.set_sensitive(True)
                if row == len(self.selected_channels_model)-1:
                    self.button_move_channel_down.set_sensitive(False)
                else:
                    self.button_move_channel_down.set_sensitive(True)

            else:
                filter = model
                model = filter.get_model()
                self.selected_channel = model.get_value(filter.convert_iter_to_child_iter(iter), 0)
                self.button_move_channel_up.set_sensitive(False)
                self.button_move_channel_down.set_sensitive(False)
        else:
            self.channel_properties_button.set_sensitive(False)
            self.button_remove_channel.set_sensitive(False)
            self.button_add_channel.set_sensitive(False)
            self.button_move_channel_up.set_sensitive(False)
            self.button_move_channel_down.set_sensitive(False)

    def on_grabber_command_entry_changed(self, entry):
        self.config.grabber_command = entry.get_text()

    def on_grabber_command_entry_activate(self, entry):
        self.xmltvfile.download()

    def on_output_file_entry_changed(self, entry):
        self.config.xmltv_file = entry.get_text()

    def on_output_file_entry_activate(self, entry):
        self.xmltvfile.load()

    def on_browse_button_clicked(self, button):
        xml_filter = gtk.FileFilter()
        xml_filter.set_name(_("XML files"))
        xml_filter.add_pattern("text/xml")
        filters = [xml_filter]
        fd = gui.FileChooserDialog(_("Select XMLTV file..."), filters)
        response = fd.run()
        if response == gtk.RESPONSE_OK:
            self.config.xmltv_file = fd.get_filename()
            self.xmltvfile.load()
            fd.hide()
        elif response == gtk.RESPONSE_CANCEL:
            fd.hide()

    def on_current_programs_checkbutton_toggled(self, checkbutton):
        self.config.display_current_programs = checkbutton.get_active()

    def on_upcoming_programs_checkbutton_toggled(self, checkbutton):
        active = checkbutton.get_active()
        self.config.display_upcoming_programs = active
        self.position_hbox.set_sensitive(active)

    def on_upcoming_programs_below_radiobutton_toggled(self, radiobutton):
        active = radiobutton.get_active()
        self.config.upcoming_programs_below = active

    def on_channels_treeview_row_activated(self, treeview, path, column):
        self.on_channels_treeview_toggled(None, path, self.channels_model)

    def __channels_treeview_button_press(self, treeview, event, menu):
        if event.type == gtk.gdk.BUTTON_PRESS and event.button == 3:
            menu.popup(None, None, None, event.button, event.time)

    def on_channel_properties_button_clicked(self, button):
        cd = ChannelDialog(self.selected_channel, self)
        cd.dialog.show()

    def on_button_remove_channel_clicked(self, button):
        selection = self.selected_channels_treeview.get_selection()
        (model, iter) = selection.get_selected()
        if iter:
            channel_name = model.get_value(iter, 0)

            channel = self.listings.get_channel(channel_name)
            channel.selected = False
            self.unselected_channels_filter.refilter()
            self.listings.set_channel_selected(channel_name, False)
            self.selected_channels_model.remove(iter)
            self.pw.update()
            self.listings.save()

    def on_button_add_channel_clicked(self, button):
        selection = self.channels_treeview.get_selection()
        (filter, iter) = selection.get_selected()
        if iter:
            model = filter.get_model()
            channel = model.get_value(filter.convert_iter_to_child_iter(iter), 0)

            channel.selected = True
            self.listings.set_channel_selected(channel.name, True)
            self.selected_channels_model.append([channel.name])
            self.unselected_channels_filter.refilter()
            self.pw.update()
            self.listings.save()

    def on_button_move_channel_down_clicked(self, button):
        selection = self.selected_channels_treeview.get_selection()
        (model, iter) = selection.get_selected()
        if iter:
            row,  = model.get_path(iter)
            self.listings.selected_channels[row], self.listings.selected_channels[row+1] = \
                self.listings.selected_channels[row+1], self.listings.selected_channels[row]
            iter_next = model.get_iter(row+1)

            model.swap(iter,iter_next)
            self.selected_channels_treeview.get_selection().emit("changed")
            self.pw.update()
            self.listings.save()

    def on_button_move_channel_up_clicked(self, button):
        selection = self.selected_channels_treeview.get_selection()
        (model, iter) = selection.get_selected()
        if iter:
            row,  = model.get_path(iter)
            self.listings.selected_channels[row], self.listings.selected_channels[row-1] = \
                self.listings.selected_channels[row-1], self.listings.selected_channels[row]
            iter_prev = model.get_iter(row-1)

            model.swap(iter,iter_prev)
            self.selected_channels_treeview.get_selection().emit("changed")
            self.pw.update()
            self.listings.save()

    def __reminders_treeview_selection_changed(self, selection):
        (reminders_model, reminders_iter) = selection.get_selected()
        if reminders_iter:
            self.remove_reminder_button.set_sensitive(True)
            reminder = reminders_model.get_value(reminders_iter, 0)
            self.program_entry.set_text(reminder.program)
            channel = pango.parse_markup(reminder.channel, u"\x00")[1]
            self.channels_comboboxentry.child.set_text(channel)
            self.notify_spinbutton.set_value(reminder.notify_time)
        else:
            self.program_entry.set_text("")
            self.channels_comboboxentry.child.set_text("")
            self.notify_spinbutton.set_value(5)
            self.add_reminder_button.set_sensitive(False)
            self.update_reminder_button.set_sensitive(False)
            self.remove_reminder_button.set_sensitive(False)

    def __reminders_treeview_key_press_event(self, treeview, event):
        if event.keyval == gtk.keysyms.Delete:
            self.remove_reminder_button.emit("clicked")

    def __program_entry_changed(self, entry):
        if len(entry.get_text()) > 0:
            self.__set_reminder_buttons_sensitivity()
        else:
            self.add_reminder_button.set_sensitive(False)
            self.update_reminder_button.set_sensitive(False)

    def __channels_comboboxentry_changed(self, comboboxentry):
        self.__set_reminder_buttons_sensitivity()

    def __notify_spinbutton_value_changed(self, spinbutton):
        self.__set_reminder_buttons_sensitivity()

    def __set_reminder_buttons_sensitivity(self):
        if not self.__get_reminder() in self.reminders.reminders:
            self.add_reminder_button.set_sensitive(True)
            if gui.has_selection(self.reminders_treeview):
                self.update_reminder_button.set_sensitive(True)
        else:
            self.add_reminder_button.set_sensitive(False)
            self.update_reminder_button.set_sensitive(False)

    def __add_reminder_button_clicked(self, button):
        reminder = self.__get_reminder()
        if self.reminders.add(reminder):
            reminders_model = self.reminders_treeview.get_model()
            reminders_model.append([reminder])
            gui.queue_resize(self.reminders_treeview)
            self.reminders.save()
            self.add_reminder_button.set_sensitive(False)
            self.sd.search_treeview.get_selection().emit("changed")
            self.pw.update()

    def __get_reminder(self):
        program = self.program_entry.get_text()
        channel = self.channels_comboboxentry.child.get_text()
        channel = gobject.markup_escape_text(channel)
        notify_time = self.notify_spinbutton.get_value_as_int()
        return Reminder(program, channel, notify_time)

    def __update_reminder_button_clicked(self, button):
        selection = self.reminders_treeview.get_selection()
        (reminders_model, reminders_iter) = selection.get_selected()
        if not reminders_iter:
            self.add_reminder_button.clicked()
        else:
            current_reminder = reminders_model.get_value(reminders_iter, 0)
            new_reminder = self.__get_reminder()
            if not current_reminder is new_reminder:
                self.reminders.update(current_reminder, new_reminder)
                reminders_model.set_value(reminders_iter, 0, new_reminder)
                gui.queue_resize(self.reminders_treeview)
                self.reminders.save()
                self.add_reminder_button.set_sensitive(False)
                self.update_reminder_button.set_sensitive(False)
                self.sd.search_treeview.get_selection().emit("changed")
                self.pw.update()

    def __remove_reminder_button_clicked(self, button):
        selection = self.reminders_treeview.get_selection()
        (reminders_model, reminders_iter) = selection.get_selected()
        if reminders_iter:
            reminder = reminders_model.get_value(reminders_iter, 0)
            if self.reminders.remove(reminder):
                reminders_model.remove(reminders_iter)
                gui.queue_resize(self.reminders_treeview)
                self.reminders.save()
                self.sd.search_treeview.get_selection().emit("changed")

    def __add_config_notifications(self):
        self.config.add_notify(config.KEY_GRABBER_COMMAND,
                               self.__grabber_command_key_changed)
        self.config.add_notify(config.KEY_XMLTV_FILE,
                               self.__xmltv_file_key_changed)
        self.config.add_notify(config.KEY_DISPLAY_CURRENT_PROGRAMS,
                               self.__display_current_programs_key_changed)
        self.config.add_notify(config.KEY_DISPLAY_UPCOMING_PROGRAMS,
                               self.__display_upcoming_programs_key_changed)
        self.config.add_notify(config.KEY_UPCOMING_PROGRAMS_BELOW,
                               self.__upcoming_programs_below_key_changed)

    def __grabber_command_key_changed(self, client, cnxn_id, entry, data):
        if entry.value.type == gconf.VALUE_STRING:
            command = entry.value.to_string()
            self.grabber_command_entry.set_text(command)

    def __xmltv_file_key_changed(self, client, cnxn_id, entry, data):
        if entry.value.type == gconf.VALUE_STRING:
            xmltv_file = entry.value.to_string()
            self.xmltvfile.props.path = xmltv_file
            self.output_file_entry.set_text(xmltv_file)

    def __display_current_programs_key_changed(self, client, cnxn_id, entry,
                                               data):
        if not entry.value:
            self.current_programs_checkbutton.set_active(True)
        elif entry.value.type == gconf.VALUE_BOOL:
            value = entry.value.get_bool()
            self.current_programs_checkbutton.set_active(value)
            if value:
                self.pw.cpt.show()
            else:
                self.pw.cpt.hide()
        else:
            self.current_programs_checkbutton.set_active(True)

    def __display_upcoming_programs_key_changed(self, client, cnxn_id, entry,
                                                data):
        if not entry.value:
            self.upcoming_programs_checkbutton.set_active(True)
        elif entry.value.type == gconf.VALUE_BOOL:
            value = entry.value.get_bool()
            self.upcoming_programs_checkbutton.set_active(value)
            if value:
                self.pw.upt.show()
            else:
                self.pw.upt.hide()
        else:
            self.upcoming_programs_checkbutton.set_active(True)

    def __upcoming_programs_below_key_changed(self, client, cnxn_id, entry,
                                              data):
        if not entry.value:
            self.upcoming_programs_below_radiobutton.set_active(True)
        elif entry.value.type == gconf.VALUE_BOOL:
            value = entry.value.get_bool()
            self.upcoming_programs_below_radiobutton.set_active(value)
            self.pw.position_upcoming_programs(value)
        else:
            self.upcoming_programs_below_radiobutton.set_active(True)

    def __xmltvfile_loading(self, xmltvfile):
        self.channels_model.clear()
        self.channels_combobox_model.clear()

    def __xmltvfile_loading_done(self, xmltvfile, listings):
        if listings:
            self.listings = listings
            self.channels_combobox_model.append([_("All")])
            self.sd.set_all_as_combo_active(self.sd.channels_combobox)
        self.selected_channels_model.clear()
        for channel_name in self.listings.selected_channels:
            self.selected_channels_model.append([channel_name])

    def __xmltvfile_loaded_channel(self, xmltvfile, channel):
        self.channels_model.append([channel])
        self.channels_combobox_model.append([channel])

    def __channel_changed(self, xmltvfile, channel):
        channels_iter = self.channels_model.get_iter_first()
        for row in self.channels_model:
            if row[0] is channel:
                channels_path = self.channels_model.get_path(channels_iter)
                self.channels_model.row_changed(channels_path, channels_iter)
                break
            channels_iter = self.channels_model.iter_next(channels_iter)

    def __downloading_logo(self, xmltvfile, channel):
        self.__channel_changed(xmltvfile, channel)

    def __downloading_logo_done(self, xmltvfile, channel):
        self.__channel_changed(xmltvfile, channel)

    def show(self, uicomponent=None, verb=None):
        self.dialog.show()

class ProgramDialog:
    def __init__(self, program):
        builder = gtk.Builder()
        builder.set_translation_domain(NAME.lower())
        builder.add_from_file(gui.program_dialog_ui_file)

        self.dialog = builder.get_object("program_dialog")
        self.dialog.connect("response", lambda d, r: d.destroy())
        self.dialog.set_title(_("Details about %s") % program.title)

        builder.get_object("name_label").set_text(program.title)
        builder.get_object("channel_label").set_text(program.channel.name)
        builder.get_object("description_label").set_text(program.description)
        builder.get_object("air_time_label").set_text(("%s-%s\n%s") %
                                      (program.start_time, program.stop_time,
                                       program.date))

        channel_image = builder.get_object("channel_image")
        if program.channel.logo:
            channel_image.set_from_pixbuf(program.channel.logo)
        else:
            channel_image.set_from_icon_name(NAME.lower(),
                                             gtk.ICON_SIZE_DIALOG)

    def show(self):
        self.dialog.show()

TIMEOUT = 100
COL_TIME  = 0
COL_DESCR = 1
COL_PROG  = 2

class SearchDialog:
    def __init__(self, xmltvfile, reminders):
        xmltvfile.connect("loading-done", self.__xmltvfile_loading_done)
        self.reminders = reminders
        self.__init_ui()
        self.__timeout_id = 0

    def __init_ui(self):
        builder = gtk.Builder()
        builder.set_translation_domain(NAME.lower())
        builder.add_from_file(gui.search_dialog_ui_file)
        
        self.dialog = builder.get_object("search_dialog")
        self.details_button = builder.get_object("details_button")
        self.add_reminder_button = builder.get_object("search_add_reminderbutton")
        self.search_entry = builder.get_object("search_entry")
        self.channels_combobox = builder.get_object("channels_combobox")
        self.search_treeview = builder.get_object("search_treeview")
        self.search_model = builder.get_object("search_model")

        self.search_model_filter = builder.get_object("search_model_filter")
        self.search_model_filter.set_visible_func(self.filter)
        
        builder.get_object("search_label").set_mnemonic_widget(self.search_entry)
        builder.connect_signals(self)
        
        selection = self.search_treeview.get_selection()
        selection.connect("changed", self.__search_treeview_selection_changed)


    def init_model(self):
        for channel in self.listings.get_channel("All"):
            for program in self.listings.get_channel(channel).programs:
                time = "<b>%s-%s</b>\n<i>%s</i>" % (program.start_time,
                                                    program.stop_time,
                                                    program.date)
                
                description = "<b>%s</b>\n<i>%s</i>" % (program.markup_escaped_title,
                                                        program.channel.markup_escaped_name)
                self.search_model.append([time,description,program])

    def on_search_entry_changed(self, entry):
        if self.__timeout_id > 0:
            gobject.source_remove(self.__timeout_id)

        self.__timeout_id = gobject.timeout_add(TIMEOUT, self.search_model_filter.refilter)

    def on_channels_combobox_changed(self,combobox):
        self.search_model_filter.refilter()

    def filter(self, model, iter):
        if iter:
            program = model.get_value(iter, COL_PROG)
            if program:
                active = self.channels_combobox.get_active()
                channels_model = self.channels_combobox.get_model()
                channel = channels_model[active][0]
                if (channel == _("All")) or (channel == program.channel.name):
                    return self.search_entry.get_text().strip().lower() in program.title.lower()

    def __search_treeview_selection_changed(self, selection):
        (search_model, search_iter) = selection.get_selected()
        if search_iter:
            self.details_button.set_sensitive(True)
            program = search_model.get_value(search_iter, COL_PROG)
            reminder = Reminder(program.title,
                                program.channel.markup_escaped_name)
            if not reminder in self.reminders.reminders:
                self.add_reminder_button.set_sensitive(True)
            else:
                self.add_reminder_button.set_sensitive(False)
        else:
            self.details_button.set_sensitive(False)
            self.add_reminder_button.set_sensitive(False)

    def on_search_treeview_row_activated(self, treeview, path, column):
        program = gui.get_selected_value(treeview, COL_PROG)
        pd = ProgramDialog(program)
        pd.show()

    def on_search_closebutton_clicked(self, button):
        self.dialog.hide()

    def on_search_add_reminderbutton_clicked(self, button):
        selection = self.search_treeview.get_selection()
        (model, iter) = selection.get_selected()
        if iter:
            program = model.get_value(iter, COL_PROG)
            reminder = Reminder(program.title, program.channel.markup_escaped_name)
            if self.reminders.add(reminder):
                self.reminders.save()

    def on_details_button_clicked(self, button):
        self.on_search_treeview_row_activated(self.search_treeview,None, None)

    def __xmltvfile_loading_done(self, xmltvfile, listings):
        if listings:
            self.listings = listings
            self.search_entry.emit("changed")
            self.init_model()

    def set_all_as_combo_active(self, combobox):
        channels_model = combobox.get_model()
        channels_iter = channels_model.get_iter_first()
        for row in channels_model:
            if row[0] == _("All"):
                combobox.set_active_iter(channels_iter)
                break
            channels_iter = channels_model.iter_next(channels_iter)

    def show(self, uicomponent=None, verb=None):
        self.present()

    def present(self, time=None):
        self.search_treeview.set_cursor(0)
        self.search_entry.grab_focus()
        self.dialog.grab_focus()
        if time:
            self.dialog.present_with_time(time)
            # Ugly, but the only way I could get it to work correct
            self.dialog.present_with_time(time)
        else:
            self.dialog.present()

# vim: set sw=4 et sts=4 tw=79 fo+=l:
