# -*- coding: utf-8 -*-

# Copyright (C) 2010 Olof Kindgren <olki@src.gnome.org>

# This file is part of OnTV.

# OnTV is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# OnTV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with OnTV; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

import dbus.service
class DBusService(dbus.service.Object):
    def __init__(self, ontv_core):
        self.ontv_core = ontv_core
        bus_name = dbus.service.BusName('org.gnome.OnTV', dbus.SessionBus())
        dbus.service.Object.__init__(self, bus_name, '/DBusService')

    @dbus.service.method("org.gnome.OnTV",
                         in_signature='', out_signature='')
    def ShowSearch(self):
        self.ontv_core.show_search_dialog()

    @dbus.service.method("org.gnome.OnTV",
                         in_signature='', out_signature='')
    def ToggleWindow(self):
        self.ontv_core.toggle_program_window()
    @dbus.service.method("org.gnome.OnTV",
                         in_signature='', out_signature='')
    def UpdateListings(self):
        self.ontv_core.update_listings()
            
